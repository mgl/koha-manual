.. include:: images.rst

.. _using-third-party-software-with-koha-label:

Using third party software with Koha
====================================

.. _marc-edit-label:

MarcEdit
-------------------------

Many libraries like to use MarcEdit for modifications or data cleanup.
If you'd like to do this you will need to download it at:
http://marcedit.reeset.net/

    **Important**

    Many of the actions described in this chapter can be done in Koha
    using :ref:`Marc Modification Templates <marc-modification-templates-label>`, but this
    section is here for those who are used to MarcEdit.

.. _adding-a-prefix-to-call-numbers-label:

Adding a prefix to call numbers
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When bringing data into Koha, you may want to first clean it up. One
common action among Koha users is to add a prefix to the call number.

-  Open MarcEdit

   |image1089|

-  Click 'MarcEditor'

-  Go to Tools > Edit Subfield Data

   |image1090|

-  To prepend data the special character is: ^b  To simply prepend data
   to the beginning of a subfield, add ^b to the Field Data: textbox and
   the data to be appended in the Replace

   |image1091|

   -  To prepend data to the beginning of the subfield while replacing a
      text string, add ^b[string to replace] to the Field Data textbox
      and the data to be appended in the Replace With textbox.

.. _importing-excel-data-into-koha-label:

Importing Excel data into Koha
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Suppose you have records of your library in excel data sheet format and
want them to import into Koha. But how will you do this? Koha will not
let you import excel records directly. Well here is a very simple
solution for you which will let you import your excel records in Koha
easily. First, we will convert excel file into Marc file and then will
import it into Koha.

Follow the given steps to import your excel records into Koha

.. _converting-from-excel-format-into-.mrk-format-label:

Converting from Excel format into .mrk format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

First, we will convert excel format into .mrk format. For this we will
use MarcEdit. You can download it from http://marcedit.reeset.net/

Now open it and select Add-ins-->Delimited Text Translator.

|image1092|

Click Next when the following window appears.

|image1093|

Browse for your excel file.

|image1094|

Locate your excel file by choosing the format Excel File(\*.xls).

|image1095|

Similarly, fill all the other entries such as Output File, Excel Sheet
Name and check UTF-8 Encoded (if required) and Click Next.

|image1096|

Now you will be prompted for mapping the fields to recognise the fields
by standard marc format.

Suppose for Field 0 that is first column I entered Map to: 022$a( Valid
ISSN for the continuing resource) and then click on Apply.

|image1097|

    **Note**

    You can customize Indicators and all other things, for more
    information on marc21 format visit the `official library of congress
    site <http://www.loc.gov/marc/bibliographic/>`__.

Similarly map all other fields and then Click on Finish.

|image1098|

And then a window will appear indicating that your Marc Text
File(\*.mrk) has been created.

|image1099|

Click Close and we have created a .mrk file from .xls file in this step.
You can view the file by double clicking on it.

.. _convert-.mrk-file-to-.mrc-label:

Convert .mrk file to .mrc
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

We will convert .mrk file that we have created in the above step into
raw Marc format that can be directly imported into Koha.

For this again open MarcEdit and Select MARC Tools.

|image1100|

Next Select MarcMaker to convert .mrk file into .mrc format.

|image1101|

Locate your input file and name your output file. Then Click Execute.

|image1102|

And it will show you the Result.

|image1103|

Click Close and now we have raw Marc records with us (.mrc file).

.. _import-.mrc-into-koha-label:

Import .mrc into Koha
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

More information on importing records into Koha can be found in the
:ref:`Stage MARC records for import <stage-marc-records-for-import-label>` 
section of this manual.

Finally we will import above created .mrc file into Koha.

Click on Tools in your Koha staff client.

|image1104|

Next Click on Stage MARC Records for Import.

|image1105|

After this, choose your previously created .mrc file and click on
Upload.

|image1106|

You can also add comment about file and finally click on Stage For
Import.

|image1107|

When the import is done, you will get a result something like this

|image1108|

Next, click on Manage staged records.

Here you can even change matching rules.

|image1109|

Click on Import this batch into catalog when you are done.

Thats it. After all the records get imported, check Status and it should
read "imported"

|image1110|

You can even undo the Import operation.

And within few minutes, we have imported around 10,000 records in Koha

.. _oclc-cataloging-services-label:

OCLC Cataloging Services
------------------------

Koha can be set to work with the OCLC cataloging services such as

- `WorldShare Record Manager <https://www.oclc.org/en/worldshare-record-manager.html>`_
- `Connexion <https://www.oclc.org/en/connexion.html>`_

This allows librarians to use the WorldShare Record Manager web interface or the
OCLC Connexion Client desktop software as their cataloging tool and send those
records to Koha with a single click.

.. _setting-up-oclc-service-on-koha-label:

Setting up the OCLC Connexion Daemon
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

First, you will need to set up the
:ref:`OCLC Connexion Daemon <cron-connexion-import-daemon-label>` on your
server. If you have a system administrator you will want to consult with them on
this process.

1. Find the script on your server and view its documentation.

   .. code-block:: shell
   
        /usr/share/koha/bin/connexion_import_daemon.pl --help

2. Create a configuration file. You could put this anywhere that is readable by
   the user that will be running the service, e.g.,
   ``/etc/koha/sites/my_instance/oclc_connexion.conf``. The output of the help
   command provides the details about what this file should contain. Here is an
   example:
   
   ::
   
       host: 0.0.0.0
       port: 5500
       log: /var/log/koha/my_instance/oclc_connexion.log
       koha: https://staff.mylibrary.example.com
       user: koha_staff_user_name
       password: koha_staff_user_password
       connexion_user: oclc_connexion_user_name
       connexion_password: oclc_connexion_user_password
       import_mode: direct

3. Since the configuration file contains passwords, make sure that it's only
   readable by the user running the script, and nobody else.
   
   .. code-block:: shell
   
         chmod 400 /etc/koha/sites/my_instance/oclc_connexion.conf

4. Run the script.

   - You can do this manually to test it out:
     
     .. code-block:: shell
     
         /usr/share/koha/bin/connexion_import_daemon.pl --config /etc/koha/sites/my_instance/oclc_connexion.conf

   - Or you can set up a ``systemd`` unit to keep the script running even when
     it crashes or the server reboots:

     1. Create a file at ``/etc/systemd/system/koha-oclc-connexion.service``:
     
        .. code-block::
        
            [Unit]
            Description=Koha OCLC Connexion Daemon
            After=network.target
        
            [Service]
            Type=exec
            ExecStart=/usr/share/koha/bin/connexion_import_daemon.pl --config /etc/koha/sites/my_instance/oclc_connexion.conf
            Restart=always
        
            [Install]
            WantedBy=multi-user.target
  
     2. Enable and start the service:

        .. code-block:: shell
        
            systemctl enable koha-oclc-connexion
            systemctl start koha-oclc-connexion

     3. Check the status of the service:

        .. code-block:: shell
        
            systemctl status koha-oclc-connexion

WorldShare Record Manager
~~~~~~~~~~~~~~~~~~~~~~~~~

.. _setting-up-worldshare-record-manager-label:

Setting up WorldShare Record Manager
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Log in to your WorldShare account.
2. Go to the "Metadata" tab.
3. Select "Record Manager" in the sidebar.
4. Select "User Preferences" in the sidebar.

   .. Note::
      
      Since these are *User* Preferences, they must be configured for each user
      who will be using the service.
   
   |worldshare_user_preferences.png|
   
5. Go to "Exporting – Bibliographic Records".
   
   The official documentation from OCLC for all settings under this heading is
   found at
   https://help.oclc.org/Metadata_Services/WorldShare_Record_Manager/Record_Manager_user_preferences/Exporting_Bibliographic_records#Set_preferences_for_TCP.2FIP_export .
   
6. Go to the "General" tab.
   
   |worldshare_user_preferences_export_biblio_format_utf8.png|
   
   1. Set the "Format" to "MARC 21 with UTF-8 Unicode", since Koha always uses
      UTF-8.
   2. Click "Save".
7. Go to the "TCP/IP" tab.
   
   |worldshare_user_preferences_export_biblio_tcp_ip.png|
   
   1. Enter the host name or IP address at which the OCLC connexion daemon
      service (configured :ref:`above <setting-up-oclc-service-on-koha-label>`)
      can be reached.
   2. Set the Authentication to "Login ID" and enter the ``connexion_user`` and
      ``connexion_password`` from the configuration file (created
      :ref:`above <setting-up-oclc-service-on-koha-label>`).
   3. Optionally set the connection delay, connection attempts, and other
      settings.
   4. Click "Save".

Using WorldShare Record Manager
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. _sending-record-from-worldshare-to-koha-label:

Sending a single record from the WorldShare Record Manager to Koha
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

1. Log in to your WorldShare account.
2. Go to the "Metadata" tab.
3. Select "Record Manager" in the sidebar.
4. Search for a record.
   
   |worldshare_search.png|
   
5. Click on the title of a record to open it.
6. Click on the "Record" dropdown menu, then "Send to", then "Local System (via
   TCP/IP)"
   
   |worldshare_record_menu_export.png|
   
7. After a while, depending on your "connection delay" settings, you should see
   the record in Koha.
   
   - If you used ``import_mode: direct`` in your configuration file, the record
     will be available in the catalog.
   - If you used ``import_mode: staged`` in your configuration file, the record
     will be :ref:`staged for import <stage-marc-records-for-import-label>`.

Sending multiple records from the WorldShare Record Manager to Koha
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

1. Log in to your WorldShare account.
2. Go to the "Metadata" tab.
3. Select "Record Manager" in the sidebar.
4. Search for a record.
   
   |worldshare_search.png|
   
5. Click on the title of a record to open it.
6. Click on the "Record" dropdown menu, then "Send to", then "Export List ...".
   Select a list to which to add the record.
7. Repeat from step 4 as needed.
8. Click on "Export Lists" in the sidebar.
9. Click on the name of a list to open it.
10. Click on the "Export" dropdown menu, then "Send to local system (via
    TCP/IP)".
   
   |worldshare_export_list.png|
   
11. After a while, depending on your "connection delay" settings, you should see
    the record in Koha.
   
   - If you used ``import_mode: direct`` in your configuration file, the record
     will be available in the catalog.
   - If you used ``import_mode: staged`` in your configuration file, the record
     will be :ref:`staged for import <stage-marc-records-for-import-label>`.

OCLC Connexion Client desktop software
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. _setting-up-the-oclc-desktop-client-label:

Setting up the OCLC Connexion Client desktop software
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    **Note**

    Screenshots are OCLC Connexion Client v.2.50, Koha v.3.12

To set up the OCLC Connexion desktop client to connect to Koha, go to
Tools > Options, then choose the Export tab.

|image1317|

-  Click the "Create..." button to set up a new destination, then choose
   "OCLC Gateway Export" and click OK.

   -  Enter the following information:

      -  "Host Name:" Your catalog’s appropriate IP address (from your
         :ref:`config file above <setting-up-oclc-service-on-koha-label>`)

      -  "Port:" Your catalog's appropriate port number (from your
         :ref:`config file above <setting-up-oclc-service-on-koha-label>`)

      -  "Login ID:" The cataloger's Koha login

      -  "Password:" The cataloger's Koha password

      -  "Notify Host Before Disconnect" = checked,

      -  "Timeout" = 100, "Retries" = 3, "Delay" = 0 (zero),

      -  "Send Local System Logon ID Password" = checked.

   |image1111|

-  Select "OK" when finished, and you should see your new "Gateway
   Export" option listed (The catalog's IP address and port are blacked
   out in the following screenshot)

   Gateway Export Added

-  Click on "Record Characteristics" and make sure that the
   bibliographic records are using MARC21, UTF-8 Unicode, and click OK
   to save.

   Record Characteristics configured

You should be ready to go!  To export a record from OCLC Connexion
Client to Koha, just press F5 while the record is on-screen.  The export
dialog will pop up, and you'll see Connexion attempting to talk to Koha.
 You should get a message that the record was added or overlaid,
including its biblio number, and a URL that you can copy into your web
browser to jump straight to the record.

.. _using-the-oclc-connexion-desktop-software-label:

Using the OCLC Connexion Client desktop software
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Records can be exported from Connexion either in a batch or one by one.

.. _exporting-records-one-by-one-label:

Exporting records one by one
''''''''''''''''''''''''''''

To export bibliographic records one by one, be sure your "Batch" options
are correct: from the "Tools" menu, select "Options", and select the
"Batch" tab. In the "Perform local actions in batch" area,
"Bibliographic Record Export" should be unchecked

|image1318|

When ready to export, from the "Action" menu, select "Export" or use the
F5 key. You will see a screen similar to the following if the import is
successful and if the record is new to the Catalog; you may copy & paste
the resulting URL into your Koha catalog to see the new record.

|image1319|

If the record was overlaid, you will see a message to that effect in the
"OCLC Gateway Export Status" window

|image1319|

.. _exporting-records-in-a-batch-label:

Exporting records in a batch
''''''''''''''''''''''''''''

To export bibliographic records in a batch, be sure your "Batch" options
are correct: from the "Tools" menu, select "Options", and select the
"Batch" tab. In the "Perform local actions in batch" area,
"Bibliographic Record Export" should be checked

|image1320|

When a record is ready to export, from the "Action" menu, select
"Export" or use the F5 key, and it’s export status will be "ready."

When ready to export the batch, from the "Batch" menu, select "Process
batch" and check the appropriate "Path" and "Export" boxes

|image1321|

The export will begin, and the bib records will be exported & imported
into Koha one by one; you will see "OCLC Gateway Export Status" windows,
as above, showing you the results of each export. That window will stay
there until you select "Close," and then the next record’s export/import
will begin. The process will continue until all records in the batch are
completed. Then you may or may not see the Connexion Client export
report (depending on your Client options for that).

.. _items-in-oclc-label:

Items in OCLC
'''''''''''''

If you'd like to create your item records in OCLC you can do so by
adding a 952 for each item to the bib record you're cataloging. 
The order of subfields in the 952 field doesn't matter; you may 
wish to set up a text string or macro in the OCLC Connexion Client 
desktop software for frequently used subfield strings. 
:ref:`location and item information <952-location-item-information-label>` cataloging 
guide will break down what 
subfields you can use in the 952, but at the minimum you want to have
subfield 2, a, b, and y on your items.

952
\\\\$2CLASSIFICATION$aHOMEBRANCHCODE$bHOLDINGBRANCHCODE$yITEMTYPECODE

-  The subfield 2 holds the classification code. This can be ddc for
   Dewey or lcc for Library of Congress or z for Custom. Other
   classification sources can be found in the :ref:`Classification
   Sources <classification-sources-label>` area in administration

-  Subfield a is your home library and needs to be the code for your
   home library, not the library name. You can find these codes in the
   :ref:`Libraries <libraries-label>` administration area.

-  Subfield b is your holding library and needs to be the code for your
   holding library, not the library name. You can find these codes in
   the :ref:`Libraries <libraries-label>` administration area.

-  Subfield y is your item type code. It needs to be the code, not the
   item type name. You can find these codes in the :ref:`Item
   Types <item-types-label>` administration area.

952 \\\\$2ddc$aMAIN$bMAIN$yBOOK

In addition to these required fields you can enter any other subfield
you'd like. Most libraries will enter a call number in subfield o and a
barcode in subfield p as well. Review the :ref:`Location and item information <952-location-item-information-label>` cataloging guide for a full list of subfields and values.

Overlaying a bib record from OCLC
'''''''''''''''''''''''''''''''''

To overlay a bib record from OCLC, include a 999__$c in 
the record with the Koha record number of the Koha 
bib record to be overlaid in the $c subfield. The 
999__$c is in the Koha MARC21 record and also at the 
end of the record's URL. Please note that only bibs 
can be overlaid and item records can not be overlaid, 
so the item records will be unaffected.

.. _talking-tech-label:

Talking Tech
-------------------------------------------------

Talking Tech i-tiva is a third party, proprietary, product that
libraries can subscribe to. Learn more here:
http://www.talkingtech.com/solutions/library.

.. _installation-and-setup-instructions-label:

Installation and setup instructions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Be sure you've run
installer/data/mysql/atomicupdate/Bug-4246-Talking-Tech-itiva-phone-notifications.pl
to install the required data pack (new syspref, notice placeholders and
messaging transport preferences)

To use,
:ref:`TalkingTechItivaPhoneNotification <talkingtechitivaphonenotification-label>`
system preference must be turned on.

If you wish to process PREOVERDUE or RESERVES messages, you'll need the
:ref:`EnhancedMessagingPreferences <EnhancedMessagingPreferences-label>` system
preference turned on, and patrons to have filled in a preference for
receiving these notices by phone.

For OVERDUE messages, overdue notice triggers must be configured under
More > Tools > :ref:`Overdue notice/status triggers <overdue-notice/status-triggers-label>`. Either
branch-specific triggers or the default level triggers may be used
(script will select whichever is appropriate).

.. _sending-notices-file-label:

Sending notices file
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Add the :ref:`TalkingTech\_itiva\_outbound.pl <cron-sending-notices-file-label>`
   script to your crontab

2. Add an FTP/SFTP or other transport method to send the output file to
   the i-tiva server

3. If you wish, archive the sent notices file in another directory after
   sending

Run TalkingTech\_itiva\_outbound.pl --help for more information

.. _receiving-results-file-label:

Receiving results file
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Add an FTP/SFTP or other transport method to send the Results file to
   a known directory on your Koha server

2. Add the :ref:`TalkingTech\_itiva\_inbound.pl <cron-receiving-notices-file-label>`
   script to your crontab, aimed at that directory

3. If you wish, archive the results file in another directory after
   processing

Run TalkingTech\_itiva\_inbound.pl --help for more information
